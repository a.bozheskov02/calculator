import CalcBody from "./modules/CalcBody/view/index";

export default function App() {
  return (
    <div className="m-auto mt-10 container p-4 pb-6 border-2 rounded max-w-sm bg-gray-600 shadow-xl">
      <h1 className="text-center text-4xl mb-4">Calculator</h1>
      <CalcBody />
    </div>
  );
}
