import ui from "./ui.module.scss";
import { Dispatch, SetStateAction } from "react";

type BtnProps = {
  value?: string;
  text: string;
  action: Dispatch<SetStateAction<never>>;
};

const Button = ({ value, text, action }: BtnProps) => {
  const HandleClick = (val: string | undefined) => {
    action(val);
  };

  return (
    <button className={`${ui.btn}`} onClick={() => HandleClick(value)}>
      {text}
    </button>
  );
};

export default Button;
