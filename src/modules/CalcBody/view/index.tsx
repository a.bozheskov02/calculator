import Button from "../../../components/button";
import ui from "./ui.module.scss";
import { useEffect, useState } from "react";

const CalcBody = () => {
  const numberBtns: string[] = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
  ];
  const actionBtns: string[] = ["%", "+", "-", "*", "/"];
  const [expression, setExpression] = useState<string>("0");

  useEffect(() => {
    if (expression === "") setExpression(() => "0");
  }, [expression]);

  const enterNumber = (val: string): void => {
    setExpression((prev: string) => {
      const lastSymbol = prev.slice(-1);

      if (prev === "0") return val;
      if ((lastSymbol === "-" || lastSymbol === "+") && val === "0")
        return prev;

      return prev + val;
    });
  };
  const performAction = (val: string): void => {
    setExpression((prev: string) => {
      const lastSymbol = prev.slice(-1);
      if (isNaN(+lastSymbol)) return prev.slice(0, -1) + val;
      if (prev === "0" && val === "-") return val;

      return prev + val;
    });
  };

  return (
    <>
      <input
        type="text"
        disabled
        value={expression}
        className="block w-full p-4 text-gray-900 border border-gray-300
               rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 f
               dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white text-right"
      />

      <div className={`${ui.buttons}`}>
        {numberBtns.map((item) => {
          return (
            <Button
              action={enterNumber}
              key={item}
              value={item}
              text={`${item}`}
            />
          );
        })}
        <Button
          action={() =>
            setExpression((prev) => {
              if (expression === "Infinity") return "";
              return prev.slice(0, -1);
            })
          }
          text={`CE`}
        />
        <Button action={() => setExpression(() => "")} text={`C`} />
      </div>

      <div className={`${ui.buttons}`}>
        {actionBtns.map((item) => {
          return (
            <Button
              action={performAction}
              key={item}
              value={item}
              text={`${item}`}
            />
          );
        })}
      </div>
      <div
        onClick={() =>
          setExpression(() => {
            try {
              return `${eval(expression)}`;
            } catch {
              return "";
            }
          })
        }
        className={`bg-green-600 text-white rounded-md p-4 
                    flex items-center justify-center 
                    text-2xl cursor-pointer`}
      >
        =
      </div>
    </>
  );
};

export default CalcBody;
